module org.example.cutlerystore {
    requires javafx.controls;
    requires javafx.fxml;
    requires static lombok;
    requires mysql.connector.j;
    requires java.sql;
    requires org.hibernate.orm.core;
    requires jakarta.persistence;
    requires java.naming;
    requires jakarta.transaction;


    opens org.example.cutlerystore to javafx.fxml;
    opens org.example.cutlerystore.fxController to javafx.fxml;
    exports org.example.cutlerystore;
    opens org.example.cutlerystore.store to javafx.fxml, org.hibernate.orm.core, jakarta.persistence;
    exports org.example.cutlerystore.store to javafx.fxml, org.hibernate.orm.core, jakarta.persistence;
    exports org.example.cutlerystore.fxController to javafx.fxml;
    opens org.example.cutlerystore.store.product to javafx.fxml, org.hibernate.orm.core, jakarta.persistence;
    exports org.example.cutlerystore.store.product to javafx.fxml, org.hibernate.orm.core, jakarta.persistence;
}