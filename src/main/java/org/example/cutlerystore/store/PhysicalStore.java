package org.example.cutlerystore.store;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.example.cutlerystore.store.product.*;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class PhysicalStore
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;
    private String address;
    @OneToMany(mappedBy = "physicalStore", cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Product> productList;
    @OneToMany(mappedBy = "physicalStore", cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Employee> employees;
    @Enumerated(EnumType.STRING)
    private City city;

    public PhysicalStore(String name, String address, City city)
    {
        this.name = name;
        this.address = address;
        this.city = city;
        this.productList = new ArrayList<>();
        this.employees = new ArrayList<>();
    }

    public Blacksmith getBlacksmith()
    {
        for(Employee employee : employees)
        {
            if(employee instanceof Blacksmith)
                return (Blacksmith) employee;
        }
        return null;
    }

    public void setBlacksmith(Blacksmith blacksmith)
    {
        if(employees.size() == 0)
        {
            employees.add(0, blacksmith);
            return;
        }
        employees.remove((Employee) getBlacksmith());
        employees.add(0, blacksmith);
    }

    @Override
    public String toString() {
        return name;
    }
}
