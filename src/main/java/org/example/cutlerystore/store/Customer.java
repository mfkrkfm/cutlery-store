package org.example.cutlerystore.store;

//Customer class defines customer in store model

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.OneToMany;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Customer extends User
{
    private String billingAddress;
    private String deliveryAddress;
    @OneToMany(mappedBy = "customer", cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Cart> customerCarts = new ArrayList<>();

    public Customer(String name, String surname, String login, String password, String billingAddress, String deliveryAddress)
    {
        super(name, surname, login, password);
        this.billingAddress = billingAddress;
        this.deliveryAddress = deliveryAddress;
    }

    public String getBillingAddress()
    {
        return billingAddress;
    }

    public String getDeliveryAddress()
    {
        return deliveryAddress;
    }

    public void displayInfo()
    {
        System.out.println("\nYour name:\t" + name);
        System.out.println("Your surname:\t" + surname);
        System.out.println("Your login:\t" + login);
        System.out.println("Your password:\t" + password);
        System.out.println("Your billing address:\t" + billingAddress);
        System.out.println("Your delivery address:\t" + deliveryAddress);
    }
}
