package org.example.cutlerystore.store;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Employee extends User
{
    private boolean isWebstoreAdmin;
    @ManyToOne
    private PhysicalStore physicalStore;
    @OneToMany(mappedBy = "employee", cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Cart> managedCarts = new ArrayList<>();

    public Employee(String name, String surname, String login, String password, boolean isWebstoreAdmin)
    {
        super(name, surname, login, password);
        this.isWebstoreAdmin = isWebstoreAdmin;
    }

    public boolean isWebstoreAdmin()
    {
        return isWebstoreAdmin;
    }

    public void setWebstoreAdmin(boolean webstoreAdmin)
    {
        isWebstoreAdmin = webstoreAdmin;
    }
    public boolean isWebstoreOnly()
    {
        if(physicalStore == null)
            return true;
        return false;
    }
}
