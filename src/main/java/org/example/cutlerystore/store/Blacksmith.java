package org.example.cutlerystore.store;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@Entity
public class Blacksmith extends Employee
{
    public Blacksmith(String name, String surname, String login, String password)
    {
        super(name, surname, login, password, true);
    }
    @Override
    public String toString() {
        if(name.equals("Brokk") || name.equals("Sindri") || name.equals("Brokkas") || name.equals("Sindretti"))
            return name + ": " + login;
        else
            return name + " " + surname + ": " + login;
    }
}
