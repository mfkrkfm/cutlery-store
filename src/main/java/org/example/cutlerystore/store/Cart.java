package org.example.cutlerystore.store;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.example.cutlerystore.store.product.*;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Cart
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private double totalPrice;
    @ManyToOne
    private Customer customer;
    @ManyToOne
    private Employee employee;
    @OneToMany(mappedBy = "cart", cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Product> products;
    private LocalDate orderCreationDate;
    @OneToMany(mappedBy = "chat", cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Comment> chat;

    public Cart(Customer customer, List<Product> products)
    {
        this.customer = customer;
        this.products = products;
        this.orderCreationDate = LocalDate.now();
        this.chat = new ArrayList<>();
        for(Product p : products)
        {
            this.totalPrice += p.getPrice();
        }
    }

    @Override
    public String toString() {
        return customer.getLogin() + ": " + Double.toString(totalPrice);
    }
}
