package org.example.cutlerystore.store;

import org.example.cutlerystore.store.product.*;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Comment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String commentTitle;
    private String commentBody;
    @ManyToOne
    private User owner;
    @OneToMany(mappedBy = "parentComment", cascade = CascadeType.ALL, orphanRemoval = true)
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Comment> replies;
    @ManyToOne
    private Comment parentComment;
    private double rating;
    @ManyToOne
    private Product product;
    @ManyToOne
    private Cart chat;
    private LocalDate datePosted;

    public Comment(String commentTitle, String commentBody, double rating, User user, Product product) {
        this.commentTitle = commentTitle;
        this.commentBody = commentBody;
        this.rating = rating;
        this.owner = user;
        this.product = product;
        this.datePosted = LocalDate.now();
    }

    public Comment(String commentTitle, String commentBody, User owner, Comment parentComment) {
        this.commentTitle = commentTitle;
        this.commentBody = commentBody;
        this.owner = owner;
        this.parentComment = parentComment;
        this.datePosted = LocalDate.now();
    }

    public Comment(String commentBody, User owner, Cart chat)
    {
        this.commentBody = commentBody;
        this.owner = owner;
        this.chat = chat;
        this.datePosted = LocalDate.now();
    }

    @Override
    public String toString() {
        if(commentTitle == null) {
            return "(" + datePosted + ") " + owner.getLogin() + ":" + commentBody;
        }
        return "(" + datePosted + ") " + owner.getLogin() + ":\n" + commentTitle + "\n" + commentBody;
    }
}
