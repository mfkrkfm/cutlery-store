package org.example.cutlerystore.store.product;

import jakarta.persistence.Entity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.example.cutlerystore.store.product.Product;

//Knife class defines org.example.cutlerystore.store.product of cutlery store
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
abstract public class Knife extends Product
{
    protected double bladeLength;
    protected String bladeType;
    protected String steel;
    protected String handleMaterial;

    public Knife(Knife other) {
        super(other);
        this.bladeLength = other.bladeLength;
        this.bladeType = other.bladeType;
        this.steel = other.steel;
        this.handleMaterial = other.handleMaterial;
    }

    public Knife(String name, String brand, double price, int quantity, String description, String countryOfOrigin, double bladeLength, String bladeType, String steel, String handleMaterial)
    {
        super(name, brand, price, quantity, description, countryOfOrigin);
        this.bladeLength = bladeLength;
        this.bladeType = bladeType;
        this.steel = steel;
        this.handleMaterial = handleMaterial;
        this.countryOfOrigin = countryOfOrigin;
    }

    public double getBladeLength()
    {
        return bladeLength;
    }

    public String getBladeType()
    {
        return bladeType;
    }

    public String getSteel()
    {
        return steel;
    }

    public String getHandleMaterial()
    {
        return handleMaterial;
    }

    public void setBladeLength(double bladeLength)
    {
        this.bladeLength = bladeLength;
    }

    public void setBladeType(String bladeType)
    {
        this.bladeType = bladeType;
    }

    public void setSteel(String steel)
    {
        this.steel = steel;
    }

    public void setHandleMaterial(String handleMaterial)
    {
        this.handleMaterial = handleMaterial;
    }

    public void displayKnife()
    {

        System.out.println("Blade Length:\t" + bladeLength + "mm");
        System.out.println("Blade Type:\t" + bladeType);
        System.out.println("Steel:\t" + steel);
        System.out.println("Handle:\t" + handleMaterial);
        System.out.println("Country of origin:\t" + countryOfOrigin);
    }
}
