package org.example.cutlerystore.store.product;

import jakarta.persistence.Entity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class FixedKnife extends Knife
{
    protected String sheathMaterial;
    public FixedKnife(String name, String brand, double price, int quantity, String description, String countryOfOrigin, double bladeLength, String bladeType, String steel, String handleMaterial, String sheathMaterial)
    {
        super(name, brand, price, quantity, description, countryOfOrigin, bladeLength, bladeType, steel, handleMaterial);
        this.sheathMaterial = sheathMaterial;
    }

    public FixedKnife(FixedKnife other) {
        super(other);
        this.sheathMaterial = other.sheathMaterial;
    }

    public String getSheathMaterial()
    {
        return sheathMaterial;
    }

    public void setSheathMaterial(String sheathMaterial)
    {
        this.sheathMaterial = sheathMaterial;
    }
}
