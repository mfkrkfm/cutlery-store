package org.example.cutlerystore.store.product;

import jakarta.persistence.Entity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class FoldingKnife extends Knife
{
    protected boolean isAutomatic;
    protected boolean hasClip;
    protected String lockType;
    public FoldingKnife(String name, String brand, double price, int quantity, String description, String countryOfOrigin, double bladeLength, String bladeType, String steel, String handleMaterial, boolean isAutomatic, boolean hasClip, String lockType)
    {
        super(name, brand, price, quantity, description, countryOfOrigin, bladeLength, bladeType, steel, handleMaterial);
        this.isAutomatic = isAutomatic;
        this.hasClip = hasClip;
        this.lockType = lockType;
    }

    public FoldingKnife(FoldingKnife other) {
        super(other);
        this.isAutomatic = other.isAutomatic;
        this.hasClip = other.hasClip;
        this.lockType = other.lockType;
    }

    public boolean isAutomatic()
    {
        return isAutomatic;
    }

    public void setAutomatic(boolean automatic)
    {
        isAutomatic = automatic;
    }

    public boolean isHasClip()
    {
        return hasClip;
    }

    public void setHasClip(boolean hasClip)
    {
        this.hasClip = hasClip;
    }

    public String getLockType()
    {
        return lockType;
    }

    public void setLockType(String lockType)
    {
        this.lockType = lockType;
    }
}
