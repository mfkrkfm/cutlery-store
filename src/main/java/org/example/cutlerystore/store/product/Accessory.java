package org.example.cutlerystore.store.product;

import jakarta.persistence.Entity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@Entity
public class Accessory extends Product
{
    public Accessory(String name, String brand, double price, int quantity, String description, String countryOfOrigin)
    {
        super(name, brand, price, quantity, description, countryOfOrigin);
    }

    public Accessory(Accessory other)
    {
        super(other);
    }
}
