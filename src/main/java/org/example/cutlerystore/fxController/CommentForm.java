package org.example.cutlerystore.fxController;

import jakarta.persistence.EntityManagerFactory;
import javafx.fxml.FXML;
import javafx.scene.control.Slider;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.example.cutlerystore.hibernate.GenericHibernate;
import org.example.cutlerystore.store.Comment;
import org.example.cutlerystore.store.User;
import org.example.cutlerystore.store.product.Product;

public class CommentForm
{
    public TextField commentTitleField;
    @FXML
    public TextArea commentBodyField;
    @FXML
    public Slider ratingField;

    private EntityManagerFactory entityManagerFactory;
    private User currentUser;
    private Product product;
    private GenericHibernate genericHibernate;
    private Comment comment;
    private boolean isUpdate;
    private SaveSuccessfulCallback saveSuccessfulCallback;


    public void setData(EntityManagerFactory entityManagerFactory, User user, Product product, SaveSuccessfulCallback callback) {
        this.entityManagerFactory = entityManagerFactory;
        this.currentUser = user;
        this.product = product;
        this.genericHibernate = new GenericHibernate(entityManagerFactory);
        this.saveSuccessfulCallback = callback;
    }

    public void setData(EntityManagerFactory entityManagerFactory, Comment comment, User user, SaveSuccessfulCallback callback) {
        this.entityManagerFactory = entityManagerFactory;
        this.comment = comment;
        this.currentUser = user;
        this.genericHibernate = new GenericHibernate(entityManagerFactory);
        this.saveSuccessfulCallback = callback;
    }

    public void setData(EntityManagerFactory entityManagerFactory, Comment comment, SaveSuccessfulCallback callback) {
        this.entityManagerFactory = entityManagerFactory;
        this.genericHibernate = new GenericHibernate(entityManagerFactory);
        this.comment = comment;
        this.isUpdate = true;
        this.saveSuccessfulCallback = callback;

        Comment commentToUpdate = genericHibernate.getEntityById(Comment.class, comment.getId());
        ratingField.setVisible(false);
        commentTitleField.setText(commentToUpdate.getCommentTitle());
        commentBodyField.setText(commentToUpdate.getCommentBody());
    }
    public boolean saveComment() {
        if (!isUpdate && comment == null) {
            Product currentProduct = genericHibernate.getEntityById(Product.class, product.getId());
            Comment reviewForProduct = new Comment(commentTitleField.getText(), commentBodyField.getText(), ratingField.getValue(), currentUser, currentProduct);
            currentProduct.getComments().add(reviewForProduct);
            genericHibernate.update(currentProduct);
        } else if (!isUpdate) {
            Comment parentComment = genericHibernate.getEntityById(Comment.class, comment.getId());
            Comment replyToComment = new Comment(commentTitleField.getText(), commentBodyField.getText(), currentUser, parentComment);
            parentComment.getReplies().add(replyToComment);
            genericHibernate.update(parentComment);
        } else {
            Comment commentToUpdate = genericHibernate.getEntityById(Comment.class, comment.getId());
            commentToUpdate.setCommentBody(commentBodyField.getText());
            commentToUpdate.setCommentTitle(commentTitleField.getText());
            genericHibernate.update(commentToUpdate);
        }
        if (saveSuccessfulCallback != null) {
            saveSuccessfulCallback.onCommentSaved();  // Call the callback after saving
        }
        Stage stage = (Stage) commentBodyField.getScene().getWindow();
        stage.close();
        return true;
    }
    public interface SaveSuccessfulCallback {
        void onCommentSaved();
    }
}
