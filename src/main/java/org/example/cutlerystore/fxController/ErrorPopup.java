package org.example.cutlerystore.fxController;

import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class ErrorPopup
{
    public Button okButton;
    public Text errorText;

    public void setData(String string)
    {
        errorText.setText(string);
    }
    public void closePopup(ActionEvent actionEvent)
    {
        Stage stage = (Stage) okButton.getScene().getWindow();
        stage.close();
    }
}
