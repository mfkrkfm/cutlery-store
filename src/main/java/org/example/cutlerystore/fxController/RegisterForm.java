package org.example.cutlerystore.fxController;

import jakarta.persistence.EntityManagerFactory;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.example.cutlerystore.HelloApplication;
import org.example.cutlerystore.hibernate.ShopHibernate;
import org.example.cutlerystore.store.Blacksmith;
import org.example.cutlerystore.store.Customer;
import org.example.cutlerystore.store.Employee;
import org.example.cutlerystore.store.User;

import java.io.IOException;

import static org.example.cutlerystore.utils.Hashing.hash;

public class RegisterForm
{
    public TextField nameTextField;
    public TextField loginTextField;
    public TextField surnameTextField;
    public PasswordField passwordTextField;
    public PasswordField repeatPasswordTextField;
    public TextField billingAddressTextField;
    public TextField deliveryAddressTextField;
    public PasswordField securityCodeField;
    public Button backToCustomerCreationButton;
    public Button registerEmployeeButton;
    private EntityManagerFactory entityManagerFactory;
    private ShopHibernate shopHibernate;

    public void setData(EntityManagerFactory entityManagerFactory)
    {
        this.entityManagerFactory = entityManagerFactory;
        this.shopHibernate = new ShopHibernate(entityManagerFactory);
        backToCustomerCreation();
    }

    public int checkCustomerTextFields()
    {
        if(nameTextField.getText().isEmpty())
            return 0;
        if(surnameTextField.getText().isEmpty())
            return 0;
        if(loginTextField.getText().isEmpty())
            return 0;
        if(passwordTextField.getText().isEmpty())
            return 0;
        if(repeatPasswordTextField.getText().isEmpty())
            return 0;
        if(billingAddressTextField.getText().isEmpty())
            return 0;
        if(deliveryAddressTextField.getText().isEmpty())
            return 0;
        if(!passwordTextField.getText().equals(repeatPasswordTextField.getText()))
            return -1;
        else
            return 1;
    }
    public int checkEmployeeTextFields()
    {
        if(nameTextField.getText().isEmpty())
            return 0;
        if(surnameTextField.getText().isEmpty())
            return 0;
        if(loginTextField.getText().isEmpty())
            return 0;
        if(passwordTextField.getText().isEmpty())
            return 0;
        if(repeatPasswordTextField.getText().isEmpty())
            return 0;
        if(!passwordTextField.getText().equals(repeatPasswordTextField.getText()))
            return -1;
        else
            return 1;
    }
    public void registerUser(ActionEvent actionEvent) throws IOException
    {
        if(checkCustomerTextFields() < 0)
        {
            showErrorPopup("Passwords do not match");
            return;
        }
        if(checkCustomerTextFields() == 0)
        {
            showErrorPopup("Empty Fields");
            return;
        }
        Customer customer = new Customer(
                nameTextField.getText(), surnameTextField.getText(), loginTextField.getText(),
                hash(passwordTextField.getText()), billingAddressTextField.getText(), deliveryAddressTextField.getText()
        );
        shopHibernate.create(customer);
        launchShop((User)customer);
    }

    public void showErrorPopup(String string)
    {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(HelloApplication.class.getResource("error-popup.fxml"));
            Parent root = fxmlLoader.load();
            ErrorPopup errorPopup = fxmlLoader.getController();
            errorPopup.setData(string);
            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.setTitle("Error");
            stage.setResizable(false);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void launchShop(User user) throws IOException
    {
        FXMLLoader fxmlLoader = new FXMLLoader(HelloApplication.class.getResource("main-window.fxml"));
        Parent parent = fxmlLoader.load();
        MainWindow mainWindow = fxmlLoader.getController();
        mainWindow.setData(entityManagerFactory, user);
        Stage stage = (Stage) nameTextField.getScene().getWindow();
        stage.setScene(new Scene(parent));
        stage.setTitle("The Dwarven Forge");
        stage.setResizable(false);
        stage.show();
    }

    public void employeeCreation(ActionEvent actionEvent)
    {
        backToCustomerCreationButton.setVisible(true);
        registerEmployeeButton.setVisible(true);
        securityCodeField.setVisible(true);
    }

    public void backToCustomerCreation()
    {
        backToCustomerCreationButton.setVisible(false);
        registerEmployeeButton.setVisible(false);
        securityCodeField.setVisible(false);
    }

    public void registerEmployee(ActionEvent actionEvent) throws IOException
    {
        if(checkEmployeeTextFields() < 0)
        {
            showErrorPopup("Passwords do not match");
            return;
        }
        if(checkEmployeeTextFields() == 0)
        {
            showErrorPopup("Empty Fields");
            return;
        }
        if(!securityCodeField.getText().isEmpty())
        {
            if(securityCodeField.getText().equals("blacksmithCode"))
            {
                Blacksmith blacksmith = new Blacksmith(
                        nameTextField.getText(), surnameTextField.getText(), loginTextField.getText(),
                        hash(passwordTextField.getText())
                );
                shopHibernate.create(blacksmith);
                launchShop((User)blacksmith);
            }
            if(securityCodeField.getText().equals("adminCode"))
            {
                Employee employee = new Employee(
                        nameTextField.getText(), surnameTextField.getText(), loginTextField.getText(),
                        hash(passwordTextField.getText()), true
                );
                shopHibernate.create(employee);
                launchShop((User)employee);
            }
            if(securityCodeField.getText().equals("employeeCode"))
            {
                Employee employee = new Employee(
                        nameTextField.getText(), surnameTextField.getText(), loginTextField.getText(),
                        hash(passwordTextField.getText()), false
                );
                shopHibernate.create(employee);
                launchShop(employee);
            }
        }
        else
        {
            if(securityCodeField.getText().isEmpty())
                showErrorPopup("Invalid Security Code");
        }
    }
}
