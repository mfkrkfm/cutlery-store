package org.example.cutlerystore.fxController;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.example.cutlerystore.HelloApplication;
import org.example.cutlerystore.store.Customer;

import java.io.IOException;
import java.time.YearMonth;
import java.util.regex.Pattern;

public class PaymentForm
{
    public TextField cardNumberTextField;
    public TextField nameSurnameTextField;
    public TextField cvvTextField;
    public Customer customer;
    public TextField monthTextField;
    public TextField yearTextField;

    public interface BuyProductCallback {
        void onPaymentSuccess();
    }
    private BuyProductCallback buyProductCallback;

    public void setData(Customer customer, BuyProductCallback buyProductCallback)
    {
        this.customer = customer;
        nameSurnameTextField.setText(customer.getName() + " " + customer.getSurname());
        this.buyProductCallback = buyProductCallback;
    }

    public boolean checkFields()
    {
        if(cardNumberTextField.getText().isEmpty())
            return false;
        if(nameSurnameTextField.getText().isEmpty())
            return false;
        if(cvvTextField.getText().isEmpty())
            return false;
        if(monthTextField.getText().isEmpty())
            return false;
        if(yearTextField.getText().isEmpty())
            return false;
        return true;
    }

    public void pay(ActionEvent actionEvent)
    {
        if(!checkFields())
        {
            showErrorPopup("Empty Fields", "Error");
            return;
        }
        if (isCreditCardNumber(cardNumberTextField.getText()))
        {
            cardNumberTextField.setText(formatCreditCardNumber(cardNumberTextField.getText()));
            if(!cvvTextField.getText().matches("\\d{3}"))
            {
                showErrorPopup("Invalid CVV", "Error");
                return;
            }
            if(!checkMonthYear(monthTextField.getText(), yearTextField.getText()))
            {
                showErrorPopup("Invalid Expiry Date", "Error");
                return;
            }
            if (!nameSurnameTextField.getText().matches("^[A-Za-z]+\\s[A-Za-z]+$"))
            {
                showErrorPopup("Invalid Name Surname", "Error");
                return;
            }
            if(buyProductCallback != null)
            {
                buyProductCallback.onPaymentSuccess();
            }
            Stage stage = (Stage) cvvTextField.getScene().getWindow();
            stage.close();
            showErrorPopup("Successfully Payed", "Success");
        }
        else
            showErrorPopup("Invalid Card Number", "Error");

    }

    public static boolean checkMonthYear(String monthText, String yearText) {
        try {
            int month = Integer.parseInt(monthText);
            int year = Integer.parseInt(yearText);
            YearMonth currentYearMonth = YearMonth.now();
            YearMonth userInputYearMonth = YearMonth.of(year, month);
            return userInputYearMonth.isAfter(currentYearMonth);
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public static boolean isCreditCardNumber(String inputStr) {
        String strippedInput = inputStr.replaceAll(" ", "");
        if (!strippedInput.matches("\\d{16}")) {
            return false;
        }
        return Pattern.matches("^\\d{16}$", strippedInput);
    }

    public static String formatCreditCardNumber(String inputStr) {
        String strippedInput = inputStr.replaceAll(" ", "");
        if (isCreditCardNumber(strippedInput)) {
            StringBuilder formattedNumber = new StringBuilder();
            for (int i = 0; i < strippedInput.length(); i++) {
                if (i > 0 && i % 4 == 0) {
                    formattedNumber.append(" ");
                }
                formattedNumber.append(strippedInput.charAt(i));
            }
            return formattedNumber.toString();
        } else {
            return "Invalid credit card number format";
        }
    }

    public void showErrorPopup(String string, String title)
    {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(HelloApplication.class.getResource("error-popup.fxml"));
            Parent root = fxmlLoader.load();
            ErrorPopup errorPopup = fxmlLoader.getController();
            errorPopup.setData(string);
            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.setTitle(title);
            stage.setResizable(false);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
