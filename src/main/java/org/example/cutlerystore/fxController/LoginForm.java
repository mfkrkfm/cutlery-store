package org.example.cutlerystore.fxController;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.example.cutlerystore.HelloApplication;
import org.example.cutlerystore.hibernate.ShopHibernate;
import org.example.cutlerystore.store.User;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import static org.example.cutlerystore.utils.Hashing.hash;

public class LoginForm implements Initializable{
    public Button enterButton;
    public TextField loginTextField;
    public TextField passwordTextField;
    public Button signUpButton;
    private EntityManagerFactory entityManagerFactory;
    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        entityManagerFactory = Persistence.createEntityManagerFactory("Shop");
    }

    public void launchStore(ActionEvent actionEvent) throws IOException
    {
        ShopHibernate hibernateShop = new ShopHibernate(entityManagerFactory);
        var user = hibernateShop.getUserByCredentials(loginTextField.getText(), hash(passwordTextField.getText()));
        if (user != null)
        {
            FXMLLoader fxmlLoader = new FXMLLoader(HelloApplication.class.getResource("main-window.fxml"));
            Parent parent = fxmlLoader.load();
            MainWindow mainWindow = fxmlLoader.getController();
            mainWindow.setData(entityManagerFactory, (User) user);
            Stage stage = (Stage) enterButton.getScene().getWindow();
            stage.setScene(new Scene(parent));
            stage.setTitle("The Dwarven Forge");
            stage.setResizable(false);
            stage.show();
        }
        else
        {
            FXMLLoader fxmlLoader = new FXMLLoader(HelloApplication.class.getResource("error-popup.fxml"));
            Parent root = fxmlLoader.load();
            ErrorPopup errorPopup = fxmlLoader.getController();
            errorPopup.setData("Wrong Credentials");
            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.setTitle("Error");
            stage.setResizable(false);
            stage.show();
        }
    }
    public void startRegistration(ActionEvent actionEvent) throws IOException
    {
        FXMLLoader fxmlLoader = new FXMLLoader(HelloApplication.class.getResource("register-form.fxml"));
        Parent parent = fxmlLoader.load();
        RegisterForm registerForm = fxmlLoader.getController();
        registerForm.setData(entityManagerFactory);
        Stage stage = (Stage) enterButton.getScene().getWindow();
        stage.setScene(new Scene(parent));
        stage.setTitle("Register Form");
        stage.setResizable(false);
        stage.show();
    }
}