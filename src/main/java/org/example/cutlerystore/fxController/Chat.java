package org.example.cutlerystore.fxController;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import javafx.event.ActionEvent;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import org.example.cutlerystore.hibernate.ShopHibernate;
import org.example.cutlerystore.store.Cart;
import org.example.cutlerystore.store.Comment;
import org.example.cutlerystore.store.User;

public class Chat
{
    public ListView<Comment> chatListView;
    public TextField messageTextField;
    private Cart cart;
    private User sender;
    private EntityManagerFactory entityManagerFactory;
    private ShopHibernate shopHibernate;

    public void setData(EntityManagerFactory entityManagerFactory, User user, Cart cart)
    {
        this.entityManagerFactory = entityManagerFactory;
        this.sender = user;
        this.cart = cart;
        this.shopHibernate = new ShopHibernate(entityManagerFactory);
        chatListView.getItems().addAll(cart.getChat());
    }

    public void sendMessage(ActionEvent actionEvent)
    {
        if(messageTextField.getText().isEmpty())
            return;
        Comment comment = new Comment(
                messageTextField.getText(), sender, cart
        );
        shopHibernate.create(comment);
        cart = shopHibernate.getEntityById(Cart.class, cart.getId());
        chatListView.getItems().clear();
        chatListView.getItems().addAll(cart.getChat());
        messageTextField.clear();
    }
}
