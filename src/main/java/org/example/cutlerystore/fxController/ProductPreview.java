package org.example.cutlerystore.fxController;

import jakarta.persistence.EntityManagerFactory;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import javafx.stage.Stage;
import org.example.cutlerystore.HelloApplication;
import org.example.cutlerystore.hibernate.ShopHibernate;
import org.example.cutlerystore.store.Comment;
import org.example.cutlerystore.store.Customer;
import org.example.cutlerystore.store.Employee;
import org.example.cutlerystore.store.User;
import org.example.cutlerystore.store.product.FixedKnife;
import org.example.cutlerystore.store.product.FoldingKnife;
import org.example.cutlerystore.store.product.Knife;
import org.example.cutlerystore.store.product.Product;

import java.io.IOException;
import java.util.List;

public class ProductPreview
{
    public TreeView<Comment> commentTree;
    public TextFlow productTextFlow;
    private EntityManagerFactory entityManagerFactory;
    private ShopHibernate shopHibernate;
    private User user;
    private Product product;
    private CommentForm.SaveSuccessfulCallback saveSuccessfulCallback;


    public void setData(EntityManagerFactory entityManagerFactory, User user, Product product)
    {
        this.entityManagerFactory = entityManagerFactory;
        this.shopHibernate = new ShopHibernate(entityManagerFactory);
        this.user = shopHibernate.getEntityById(User.class, user.getId());
        this.product = product;
        loadReviews();
        setTextFlow();
    }

    public void setTextFlow()
    {
        Text productName = new Text("Name: " + product.getName());
        Text productBrand = new Text("\nBrand: " + product.getBrand());
        Text productDescription = new Text("\nDescription:\n" + product.getDescription());
        Text productPrice = new Text("\nPrice: " + Double.toString(product.getPrice()));
        Text productQuantity = new Text("\tQuantity: " + Integer.toString(product.getQuantity()));
        Text productCountryOfOrigin = new Text("\nCountry: " + product.getCountryOfOrigin());
        productTextFlow.getChildren().add(productName);
        productTextFlow.getChildren().add(productBrand);
        productTextFlow.getChildren().add(productDescription);
        productTextFlow.getChildren().add(productPrice);
        productTextFlow.getChildren().add(productQuantity);
        productTextFlow.getChildren().add(productCountryOfOrigin);
        if(product instanceof Knife)
        {
            Knife knife = (Knife) product;
            Text knifeBladeLength = new Text("\nBlade Length: " + Double.toString(knife.getBladeLength()));
            Text knifeBladeType = new Text("\nBlade Type: " + knife.getBladeType());
            Text knifeSteel = new Text("\nSteel: " + knife.getSteel());
            Text knifeHandleMaterial = new Text("\nHandle: " + knife.getHandleMaterial());
            productTextFlow.getChildren().add(knifeBladeLength);
            productTextFlow.getChildren().add(knifeBladeType);
            productTextFlow.getChildren().add(knifeSteel);
            productTextFlow.getChildren().add(knifeHandleMaterial);
            if(knife instanceof FoldingKnife)
            {
                FoldingKnife foldingKnife = (FoldingKnife) knife;
                Text foldingKnifeLockType = new Text("\nLock Type: " + foldingKnife.getLockType() + "\n");
                Text foldingKnifeAutomatic;
                Text foldingKnifeClip;
                if(foldingKnife.isHasClip())
                    foldingKnifeClip = new Text("Clip: Yes");
                else
                    foldingKnifeClip = new Text("Clip: No");
                if(foldingKnife.isAutomatic())
                    foldingKnifeAutomatic = new Text("\tAutomatic: Yes");
                else
                    foldingKnifeAutomatic = new Text("\tAutomatic: No");
                productTextFlow.getChildren().add(foldingKnifeLockType);
                productTextFlow.getChildren().add(foldingKnifeClip);
                productTextFlow.getChildren().add(foldingKnifeAutomatic);
            }
            if(knife instanceof FixedKnife)
            {
                FixedKnife fixedKnife = (FixedKnife) knife;
                Text fixedKnifeSheathMaterial = new Text("\nSheath Material: " + fixedKnife.getSheathMaterial());
                productTextFlow.getChildren().add(fixedKnifeSheathMaterial);
            }
        }
    }

    public void loadReviews() {
        Product p = shopHibernate.getEntityById(Product.class, product.getId());
        commentTree.setRoot(new TreeItem<>());
        commentTree.setShowRoot(false);
        commentTree.getRoot().setExpanded(true);
        p.getComments().forEach(comment -> addToTree(comment, commentTree.getRoot()));
        System.out.println("\n\n\n\nReviews loaded\n\n\n\n");
    }

    public void addReview() throws IOException
    {
        FXMLLoader fxmlLoader = new FXMLLoader(HelloApplication.class.getResource("comment-form.fxml"));
        Parent parent = fxmlLoader.load();
        CommentForm commentForm = fxmlLoader.getController();
        commentForm.setData(entityManagerFactory, user, product, new CommentForm.SaveSuccessfulCallback() {
            @Override
            public void onCommentSaved() {
                loadReviews();
            }
        });
        Stage stage = new Stage();
        stage.setScene(new Scene(parent));
        stage.setTitle("Your Review");
        stage.setResizable(false);
        stage.show();
    }

    private void addToTree(Comment comment, TreeItem<Comment> parentComment) {
        TreeItem<Comment> treeItem = new TreeItem<>(comment);
        parentComment.getChildren().add(treeItem);
        comment.getReplies().forEach(sub -> addToTree(sub, treeItem));
    }

    public void updateComment() throws IOException {
        if(commentTree.getSelectionModel().getSelectedItem().getValue().getOwner().getId() != user.getId())
        {
            showErrorPopup("You can edit only your comments");
            return;
        }
        FXMLLoader fxmlLoader = new FXMLLoader(HelloApplication.class.getResource("comment-form.fxml"));
        Parent parent = fxmlLoader.load();
        CommentForm commentForm = fxmlLoader.getController();
        commentForm.setData(entityManagerFactory, commentTree.getSelectionModel().getSelectedItem().getValue(),new CommentForm.SaveSuccessfulCallback() {
            @Override
            public void onCommentSaved() {
                loadReviews();
            }
        });
        Stage stage = new Stage();
        stage.setScene(new Scene(parent));
        stage.setTitle("Your Review");
        stage.setResizable(false);
        stage.show();
    }

    public void reply() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(HelloApplication.class.getResource("comment-form.fxml"));
        Parent parent = fxmlLoader.load();
        CommentForm commentForm = fxmlLoader.getController();
        commentForm.setData(entityManagerFactory, commentTree.getSelectionModel().getSelectedItem().getValue(), user, new CommentForm.SaveSuccessfulCallback() {
            @Override
            public void onCommentSaved() {
                loadReviews();
            }
        });
        Stage stage = new Stage();
        stage.setScene(new Scene(parent));
        stage.setTitle("Your Review");
        stage.setResizable(false);
        stage.show();
    }

    public void delete() {
        if(user instanceof Customer)
        {
            if(commentTree.getSelectionModel().getSelectedItem().getValue().getOwner().getId() != user.getId())
            {
                showErrorPopup("You can delete only your comments");
                return;
            }
        }
        shopHibernate.deleteComment(commentTree.getSelectionModel().getSelectedItem().getValue().getId());
        commentTree.setRoot(null);
        loadReviews();
    }

    public void showErrorPopup(String string)
    {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(HelloApplication.class.getResource("error-popup.fxml"));
            Parent root = fxmlLoader.load();
            ErrorPopup errorPopup = fxmlLoader.getController();
            errorPopup.setData(string);
            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.setTitle("Error");
            stage.setResizable(false);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
