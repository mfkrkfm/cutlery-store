package org.example.cutlerystore.fxController;

import jakarta.persistence.EntityManagerFactory;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import javafx.stage.Stage;
import org.example.cutlerystore.HelloApplication;
import org.example.cutlerystore.hibernate.ShopHibernate;
import org.example.cutlerystore.store.*;
import org.example.cutlerystore.store.product.*;
import java.io.IOException;
import java.util.*;

import static org.example.cutlerystore.utils.Hashing.hash;

public class MainWindow
{
    //Product Tab
    public ListView<Product> productShopList;
    public ListView<Product> productAdminList;
    public RadioButton productFixedKnifeRadio;
    public RadioButton productFoldingKnifeRadio;
    public RadioButton productAccessoryRadio;
    public CheckBox foldingKnifeHasClipCB;
    public CheckBox foldingKnifeIsAutomaticCB;
    public TextField fixedKnifeSheathMaterialField;
    public TextField productCountryField;
    public TextField foldingKnifeLockTypeField;
    public TextField knifeHandleMaterialField;
    public TextField knifeSteelField;
    public TextField knifeBladeTypeField;
    public TextField knifeBladeLengthField;
    public TextArea productDescriptionField;
    public TextField productQuantityField;
    public TextField productBrandField;
    public TextField productPriceField;
    public TextField productNameField;
    public ComboBox<PhysicalStore> storeComboBox;

    //User Tab
    public TextField userNameField;
    public TextField customerBillingAddress;
    public TextField userSurnameField;
    public TextField customerDeliveryAddress;
    public TextField userLoginField;
    public TextField userPasswordField;
    public TextField userRepeatPasswordField;
    public CheckBox userAdminCheckBox;
    public RadioButton userEmployeeRadio;
    public RadioButton userCustomerRadio;
    public ListView<User> userAdminList;
    public ComboBox<PhysicalStore> usersComboBox;
    public Button addUserButton;
    public Button deleteUserButton;
    public Button updateUserButton;

    //Shop Tab
    public ListView<Product> cartProductList;
    public TextArea cartCommentsTextArea;
    public CheckBox userBlacksmithCB;
    public Tab homeTab;
    public Tab shopTab;
    public Tab productsTab;
    public Tab usersTab;
    public Tab aboutUsTab;
    public TabPane tabPane;
    public RadioButton shopFoldRadio;
    public RadioButton shopFixRadio;
    public RadioButton shopAccessoryRadio;

    //Order Tab
    public Tab ordersTab;
    public ListView<Cart> orderAdminList;
    public ListView<Product> orderProductAdminList;
    public TextField customerNameTextField;

    //Store Tab
    public ListView<PhysicalStore> physicalStoreList;
    public ListView<Product> productStoreList;
    public ListView<Employee> employeeStoreList;
    public TextField nameTextField;
    public TextField addressTextField;
    public ComboBox<City> cityComboBox;
    public ComboBox<Blacksmith> blacksmithComboBox;
    public Tab storesTab;
    
    //Account Tab
    public TextFlow userInfoTextFlow;
    public PasswordField oldPasswordTextField;
    public PasswordField newPasswordTextField;
    public PasswordField repeatNewPasswordTextField;
    public TextFlow homeTextFlow;

    //My Orders Tab
    public ListView<Cart> myOrdersListView;
    public ListView<Product> myOrdersProductsListView;
    public Button cancelOrderButton;
    public Button unnasignOrderButton;
    public Button sendOrderButton;


    private EntityManagerFactory entityManagerFactory;
    public ShopHibernate shopHibernate;
    public User user;

    public void loadTabData()
    {
        productShopList.getItems().addAll(shopHibernate.loadAvailableProducts());
        userAdminList.getItems().addAll(shopHibernate.getAllRecords(User.class));
        productAdminList.getItems().addAll(shopHibernate.loadAvailableProducts());
        orderAdminList.getItems().addAll(shopHibernate.loadAvailableCarts());
        physicalStoreList.getItems().addAll(shopHibernate.getAllRecords(PhysicalStore.class));
        storeComboBox.getItems().addAll(shopHibernate.getAllRecords(PhysicalStore.class));
        usersComboBox.getItems().addAll(shopHibernate.getAllRecords(PhysicalStore.class));
        blacksmithComboBox.getItems().addAll(shopHibernate.loadBlacksmiths());
        cityComboBox.getItems().addAll(Arrays.asList(City.values()));
        Text text = new Text(user.getName() + " " + user.getSurname() + "\nLogin: " + user.getLogin());
        userInfoTextFlow.getChildren().add(text);
        if(user instanceof Customer)
        {
            myOrdersListView.getItems().addAll(((Customer) user).getCustomerCarts());
            Text customerText = new Text(
                    "\nBilling Address: " + ((Customer) user).getBillingAddress()
                    + "\nDelivery Address: " + ((Customer) user).getDeliveryAddress()
            );
            userInfoTextFlow.getChildren().add(customerText);
        }
        if(user instanceof Employee || user instanceof Blacksmith)
        {
            myOrdersListView.getItems().addAll(((Employee) user).getManagedCarts());
            Text employeeText = new Text(
                    "\nPhysical Store: " + ((Employee) user).getPhysicalStore() +
                    "\nAdmin: " + ((Employee) user).isWebstoreAdmin()
            );
            userInfoTextFlow.getChildren().add(employeeText);
        }
    }

    public void setData(EntityManagerFactory entityManagerFactory, User user) {
        this.entityManagerFactory = entityManagerFactory;
        this.shopHibernate = new ShopHibernate(entityManagerFactory);
        this.user = user;
        loadTabData();
        setUserView();
    }

    private void setUserView()
    {
        if(user instanceof Customer)
        {
            tabPane.getTabs().remove(productsTab);
            tabPane.getTabs().remove(usersTab);
            tabPane.getTabs().remove(ordersTab);
            tabPane.getTabs().remove(storesTab);
            sendOrderButton.setVisible(false);
            unnasignOrderButton.setVisible(false);
        }
        if(user instanceof Employee && !((Employee) user).isWebstoreAdmin())
        {
            addUserButton.setVisible(false);
            deleteUserButton.setVisible(false);
            updateUserButton.setVisible(false);
        }
    }

    public boolean checkProductTextFields()
    {
        if(productNameField.getText().isEmpty())
            return false;
        if(productBrandField.getText().isEmpty())
            return false;
        if(productPriceField.getText().isEmpty())
            return false;
        if(productCountryField.getText().isEmpty())
            return false;
        if(productQuantityField.getText().isEmpty())
            return false;
        if((productFixedKnifeRadio.isSelected() || productFoldingKnifeRadio.isSelected()) && knifeBladeLengthField.getText().isEmpty())
            return false;
        else
            return true;
    }

    public void resetProductRadioButtons()
    {
        //reset radio buttons
        productFixedKnifeRadio.setSelected(false);
        productFoldingKnifeRadio.setSelected(false);
        productAccessoryRadio.setSelected(false);
    }
    public void clearProductFields()
    {
        //clear text fields
        productNameField.clear();
        productBrandField.clear();
        productDescriptionField.clear();
        productPriceField.clear();
        productQuantityField.clear();
        productCountryField.clear();
        knifeBladeLengthField.clear();
        knifeBladeTypeField.clear();
        knifeSteelField.clear();
        knifeHandleMaterialField.clear();
        fixedKnifeSheathMaterialField.clear();
        foldingKnifeLockTypeField.clear();
        foldingKnifeHasClipCB.setSelected(false);
        foldingKnifeIsAutomaticCB.setSelected(false);
        //enable all fields
        knifeBladeLengthField.setDisable(false);
        knifeBladeTypeField.setDisable(false);
        knifeSteelField.setDisable(false);
        knifeHandleMaterialField.setDisable(false);
        fixedKnifeSheathMaterialField.setDisable(false);
        foldingKnifeLockTypeField.setDisable(false);
        foldingKnifeIsAutomaticCB.setDisable(false);
        foldingKnifeHasClipCB.setDisable(false);
        storeComboBox.setValue(null);
    }
    public void addRecord(ActionEvent actionEvent)
    {
        if(checkProductTextFields())
        {
            if (productFixedKnifeRadio.isSelected())
            {
                FixedKnife fixedKnife = new FixedKnife
                        (
                                productNameField.getText(), productBrandField.getText(),
                                Double.parseDouble(productPriceField.getText()),
                                Integer.parseInt(productQuantityField.getText()),
                                productDescriptionField.getText(), productCountryField.getText(),
                                Double.parseDouble(knifeBladeLengthField.getText()),
                                knifeBladeTypeField.getText(), knifeSteelField.getText(),
                                knifeHandleMaterialField.getText(), fixedKnifeSheathMaterialField.getText()
                        );
                if(storeComboBox.getSelectionModel().getSelectedItem() != null)
                    fixedKnife.setPhysicalStore(storeComboBox.getSelectionModel().getSelectedItem());
                shopHibernate.create(fixedKnife);
            }
            if (productFoldingKnifeRadio.isSelected())
            {
                FoldingKnife foldingKnife = new FoldingKnife
                        (
                                productNameField.getText(), productBrandField.getText(),
                                Double.parseDouble(productPriceField.getText()),
                                Integer.parseInt(productQuantityField.getText()),
                                productDescriptionField.getText(), productCountryField.getText(),
                                Double.parseDouble(knifeBladeLengthField.getText()),
                                knifeBladeTypeField.getText(), knifeSteelField.getText(),
                                knifeHandleMaterialField.getText(), foldingKnifeIsAutomaticCB.isSelected(),
                                foldingKnifeHasClipCB.isSelected(), foldingKnifeLockTypeField.getText()
                        );
                if(storeComboBox.getSelectionModel().getSelectedItem() != null)
                    foldingKnife.setPhysicalStore(storeComboBox.getSelectionModel().getSelectedItem());
                shopHibernate.create(foldingKnife);
            }
            if (productAccessoryRadio.isSelected())
            {
                Accessory accessory = new Accessory
                        (
                                productNameField.getText(), productBrandField.getText(),
                                Double.parseDouble(productPriceField.getText()),
                                Integer.parseInt(productQuantityField.getText()),
                                productDescriptionField.getText(), productCountryField.getText()
                        );
                if(storeComboBox.getSelectionModel().getSelectedItem() != null)
                    accessory.setPhysicalStore(storeComboBox.getSelectionModel().getSelectedItem());
                shopHibernate.create(accessory);
            }
            else if(!productFixedKnifeRadio.isSelected() && !productFoldingKnifeRadio.isSelected() && !productAccessoryRadio.isSelected())
            {
                showErrorPopup("Select product type");
            }
        }
        else
        {
            showErrorPopup("Empty Fields");
        }
        resetProductRadioButtons();
        clearProductFields();
        productAdminList.getSelectionModel().clearSelection();
        productAdminList.getItems().clear();
        productAdminList.getItems().addAll(shopHibernate.loadAvailableProducts());
        productShopList.getItems().clear();
        productShopList.getItems().addAll(shopHibernate.loadAvailableProducts());
    }

    public void deleteRecord(ActionEvent actionEvent)
    {
        shopHibernate.delete(Product.class, productAdminList.getSelectionModel().getSelectedItem().getId());
        clearProductFields();
        productAdminList.getItems().clear();
        productAdminList.getItems().addAll(shopHibernate.loadAvailableProducts());
        productShopList.getItems().clear();
        productShopList.getItems().addAll(shopHibernate.loadAvailableProducts());
        productFoldingKnifeRadio.setSelected(false);
        productFixedKnifeRadio.setSelected(false);
        productAccessoryRadio.setSelected(false);
    }

    public void updateRecord(ActionEvent actionEvent)
    {
        Product product = shopHibernate.getEntityById(Product.class, productAdminList.getSelectionModel().getSelectedItem().getId());
        if(productAdminList.getSelectionModel().getSelectedItem() != null)
            product.setPhysicalStore(storeComboBox.getSelectionModel().getSelectedItem());
        product.setName(productNameField.getText());
        product.setBrand(productBrandField.getText());
        product.setPrice(Double.parseDouble(productPriceField.getText()));
        product.setQuantity(Integer.parseInt(productQuantityField.getText()));
        product.setDescription(productDescriptionField.getText());
        product.setCountryOfOrigin(productCountryField.getText());
        if (product instanceof Knife)
        {
            Knife knife = (Knife) product;
            knife.setBladeLength(Double.parseDouble(knifeBladeLengthField.getText()));
            knife.setBladeType(knifeBladeTypeField.getText());
            knife.setSteel(knifeSteelField.getText());
            knife.setHandleMaterial(knifeHandleMaterialField.getText());
            if(knife instanceof FixedKnife)
            {
                FixedKnife fixedKnife = (FixedKnife) knife;
                fixedKnife.setSheathMaterial(fixedKnifeSheathMaterialField.getText());
                shopHibernate.update(fixedKnife);
            }
            if(knife instanceof FoldingKnife)
            {
                FoldingKnife foldingKnife = (FoldingKnife) knife;
                foldingKnife.setAutomatic(foldingKnifeIsAutomaticCB.isSelected());
                foldingKnife.setHasClip(foldingKnifeHasClipCB.isSelected());
                foldingKnife.setLockType(foldingKnifeLockTypeField.getText());
                shopHibernate.update(foldingKnife);
            }
        }
        if(product instanceof Accessory)
        {
            Accessory accessory = (Accessory) product;
            shopHibernate.update(accessory);
        }
        clearProductFields();
        resetProductRadioButtons();
        productAdminList.getItems().clear();
        productAdminList.getItems().addAll(shopHibernate.loadAvailableProducts());
        productAdminList.getSelectionModel().clearSelection();
    }

    public void disableFields(ActionEvent actionEvent)
    {
        clearProductFields();
        if(productFixedKnifeRadio.isSelected())
        {
            foldingKnifeLockTypeField.setDisable(true);
            foldingKnifeIsAutomaticCB.setDisable(true);
            foldingKnifeHasClipCB.setDisable(true);
        }
        if(productFoldingKnifeRadio.isSelected())
        {
            fixedKnifeSheathMaterialField.setDisable(true);
        }
        if(productAccessoryRadio.isSelected())
        {
            knifeBladeLengthField.setDisable(true);
            knifeBladeTypeField.setDisable(true);
            knifeSteelField.setDisable(true);
            knifeHandleMaterialField.setDisable(true);
            fixedKnifeSheathMaterialField.setDisable(true);
            foldingKnifeLockTypeField.setDisable(true);
            foldingKnifeIsAutomaticCB.setDisable(true);
            foldingKnifeHasClipCB.setDisable(true);
        }
    }

    public void loadProductData(MouseEvent mouseEvent)
    {
        clearProductFields();
        Product product = shopHibernate.getEntityById(Product.class, productAdminList.getSelectionModel().getSelectedItem().getId());
        productNameField.setText(product.getName());
        productBrandField.setText(product.getBrand());
        productDescriptionField.setText(product.getDescription());
        productPriceField.setText(Double.toString(product.getPrice()));
        productQuantityField.setText(Integer.toString(product.getQuantity()));
        productCountryField.setText(product.getCountryOfOrigin());
        if(product.getPhysicalStore() != null)
            storeComboBox.setValue(product.getPhysicalStore());
        if (product instanceof Knife)
        {
            Knife knife = (Knife) product;
            knifeBladeLengthField.setText(Double.toString(knife.getBladeLength()));
            knifeBladeTypeField.setText(knife.getBladeType());
            knifeSteelField.setText(knife.getSteel());
            knifeHandleMaterialField.setText(knife.getHandleMaterial());
            if(knife instanceof FixedKnife)
            {
                productFixedKnifeRadio.setSelected(true);
                FixedKnife fixedKnife = (FixedKnife) knife;
                fixedKnifeSheathMaterialField.setText(fixedKnife.getSheathMaterial());
                foldingKnifeIsAutomaticCB.setDisable(true);
                foldingKnifeHasClipCB.setDisable(true);
                foldingKnifeLockTypeField.setDisable(true);
            }
            if(knife instanceof FoldingKnife)
            {
                productFoldingKnifeRadio.setSelected(true);
                FoldingKnife foldingKnife = (FoldingKnife) knife;
                foldingKnifeIsAutomaticCB.setSelected(foldingKnife.isAutomatic());
                foldingKnifeHasClipCB.setSelected(foldingKnife.isHasClip());
                foldingKnifeLockTypeField.setText(foldingKnife.getLockType());
                fixedKnifeSheathMaterialField.setDisable(true);
            }
        }
        if(product instanceof Accessory)
        {
            productAccessoryRadio.setSelected(true);
            foldingKnifeIsAutomaticCB.setDisable(true);
            foldingKnifeHasClipCB.setDisable(true);
            knifeBladeLengthField.setDisable(true);
            knifeBladeTypeField.setDisable(true);
            knifeSteelField.setDisable(true);
            knifeHandleMaterialField.setDisable(true);
            fixedKnifeSheathMaterialField.setDisable(true);
            foldingKnifeLockTypeField.setDisable(true);

        }
    }

    public void showErrorPopup(String string)
    {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(HelloApplication.class.getResource("error-popup.fxml"));
            Parent root = fxmlLoader.load();
            ErrorPopup errorPopup = fxmlLoader.getController();
            errorPopup.setData(string);
            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.setTitle("Error");
            stage.setResizable(false);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void resetUserRadioButtons()
    {
        userCustomerRadio.setSelected(false);
        userEmployeeRadio.setSelected(false);
    }

    public int checkUserTextFields()
    {
        if(userNameField.getText().isEmpty())
            return 0;
        if(userSurnameField.getText().isEmpty())
            return 0;
        if(userLoginField.getText().isEmpty())
            return 0;
        if(userPasswordField.getText().isEmpty())
            return 0;
        if(userRepeatPasswordField.getText().isEmpty())
            return 0;
        if(userCustomerRadio.isSelected() && customerBillingAddress.getText().isEmpty())
            return 0;
        if(userCustomerRadio.isSelected() && customerDeliveryAddress.getText().isEmpty())
            return 0;
        if(!userPasswordField.getText().equals(userRepeatPasswordField.getText()))
            return -1;
        else
            return 1;
    }
    public void clearUserFields()
    {
        userNameField.clear();
        userSurnameField.clear();
        userLoginField.clear();
        userPasswordField.clear();
        userRepeatPasswordField.clear();
        customerDeliveryAddress.clear();
        customerBillingAddress.clear();
        userAdminCheckBox.setSelected(false);
        userAdminCheckBox.setDisable(false);
        userBlacksmithCB.setSelected(false);
        userBlacksmithCB.setDisable(false);
        customerBillingAddress.setDisable(false);
        customerDeliveryAddress.setDisable(false);
        usersComboBox.setDisable(false);
        usersComboBox.setValue(null);
    }
    public void disableUserFields(ActionEvent actionEvent)
    {
        clearUserFields();
        if(userCustomerRadio.isSelected())
        {
            userAdminCheckBox.setDisable(true);
            userBlacksmithCB.setDisable(true);
        }
        if(userEmployeeRadio.isSelected())
        {
            customerBillingAddress.setDisable(true);
            customerDeliveryAddress.setDisable(true);
        }
    }

    public void addUser(ActionEvent actionEvent)
    {
        if(checkUserTextFields() < 0)
            showErrorPopup("Password does not match");
        else if(checkUserTextFields() > 0)
        {
            if(userCustomerRadio.isSelected())
            {
                Customer customer = new Customer
                        (
                                userNameField.getText(), userSurnameField.getText(),
                                userLoginField.getText(), hash(userPasswordField.getText()),
                                customerBillingAddress.getText(), customerDeliveryAddress.getText()
                        );
                shopHibernate.create(customer);
            }
            if(userEmployeeRadio.isSelected())
            {
                if(userBlacksmithCB.isSelected())
                {
                    Blacksmith blacksmith = new Blacksmith
                            (
                                    userNameField.getText(), userSurnameField.getText(),
                                    userLoginField.getText(), hash(userPasswordField.getText())
                            );
                    shopHibernate.create(blacksmith);
                }
                else
                {
                    Employee employee = new Employee
                            (
                                    userNameField.getText(), userSurnameField.getText(),
                                    userLoginField.getText(), hash(userPasswordField.getText()),
                                    userAdminCheckBox.isSelected()
                            );
                    if(usersComboBox.getSelectionModel().getSelectedItem() != null)
                        employee.setPhysicalStore(usersComboBox.getSelectionModel().getSelectedItem());
                    shopHibernate.create(employee);
                }
            }
            else
            {
                showErrorPopup("Select user type");
            }
            userAdminList.getItems().clear();
            userAdminList.getItems().addAll(shopHibernate.getAllRecords(User.class));
        }
        else
        {
            showErrorPopup("Empty Fields");
        }
        resetUserRadioButtons();
        clearUserFields();
        userAdminList.getSelectionModel().clearSelection();
    }

    public void loadUserData()
    {
        clearUserFields();
        User user = shopHibernate.getEntityById(User.class, userAdminList.getSelectionModel().getSelectedItem().getId());
        userNameField.setText(user.getName());
        userSurnameField.setText(user.getSurname());
        userLoginField.setText(user.getLogin());
        userPasswordField.clear();
        userRepeatPasswordField.clear();
        if(user instanceof Customer)
        {
            userCustomerRadio.setSelected(true);
            userAdminCheckBox.setDisable(true);
            userBlacksmithCB.setDisable(true);
            usersComboBox.setDisable(true);
            customerBillingAddress.setText(((Customer) user).getBillingAddress());
            customerDeliveryAddress.setText(((Customer) user).getDeliveryAddress());
        }
        if(user instanceof Employee)
        {
            userEmployeeRadio.setSelected(true);
            customerBillingAddress.setDisable(true);
            customerDeliveryAddress.setDisable(true);
            userAdminCheckBox.setSelected(((Employee) user).isWebstoreAdmin());
            if(((Employee) user).getPhysicalStore() != null)
                usersComboBox.setValue(((Employee) user).getPhysicalStore());
        }
        if(user instanceof Blacksmith)
        {
            userBlacksmithCB.setSelected(true);
            if(((Blacksmith) user).getPhysicalStore() != null)
                usersComboBox.setValue(((Employee) user).getPhysicalStore());
        }
    }

    public void deleteUser(ActionEvent actionEvent)
    {
        if(userAdminList.getSelectionModel().getSelectedItem().getId() == user.getId())
            showErrorPopup("You can't delete current user");
        shopHibernate.delete(User.class, userAdminList.getSelectionModel().getSelectedItem().getId());
        clearUserFields();
        userAdminList.getItems().clear();
        userAdminList.getItems().addAll(shopHibernate.getAllRecords(User.class));
        userCustomerRadio.setSelected(false);
        userEmployeeRadio.setSelected(false);
    }

    public void updateUser(ActionEvent actionEvent)
    {
        if(checkUserTextFields() < 0)
        {
            showErrorPopup("Passwords do not match");
            return;
        }
        if(checkUserTextFields() == 0)
        {
            showErrorPopup("Empty Fields");
            return;
        }
        User user = shopHibernate.getEntityById(User.class, userAdminList.getSelectionModel().getSelectedItem().getId());
        user.setName(userNameField.getText());
        user.setSurname(userSurnameField.getText());
        user.setLogin(userLoginField.getText());
        user.setPassword(hash(userPasswordField.getText()));
        if(user instanceof Customer)
        {
            Customer customer = (Customer) user;
            customer.setBillingAddress(customerBillingAddress.getText());
            customer.setDeliveryAddress(customerDeliveryAddress.getText());
            shopHibernate.update(customer);
        }
        if(user instanceof Employee)
        {
            Employee employee = (Employee) user;
            employee.setWebstoreAdmin(userAdminCheckBox.isSelected());
            if(usersComboBox.getSelectionModel().getSelectedItem() != null)
                employee.setPhysicalStore(usersComboBox.getSelectionModel().getSelectedItem());
            shopHibernate.update(employee);
        }
        clearUserFields();
        resetUserRadioButtons();
        userAdminList.getItems().clear();
        userAdminList.getItems().addAll(shopHibernate.getAllRecords(User.class));
        userAdminList.getSelectionModel().clearSelection();
    }

    //Shop Tab

    public void cartRemoveRecord(ActionEvent actionEvent)
    {
        Product product = cartProductList.getSelectionModel().getSelectedItem();
        productShopList.getItems().add(product);
        cartProductList.getItems().remove(product);
    }

    public void cartAddRecord(ActionEvent actionEvent)
    {
        Product product = productShopList.getSelectionModel().getSelectedItem();
        cartProductList.getItems().add(product);
        productShopList.getItems().remove(product);
    }

    public void buyAction(ActionEvent actionEvent) throws IOException
    {
        if(!(user instanceof Customer))
        {
            showErrorPopup("You're not a customer");
            return;
        }
        if(cartProductList.getItems().size() == 0)
        {
            showErrorPopup("Nothing to buy");
            return;
        }
        FXMLLoader fxmlLoader = new FXMLLoader(HelloApplication.class.getResource("payment-form.fxml"));
        Parent root = fxmlLoader.load();
        PaymentForm paymentForm = fxmlLoader.getController();
        paymentForm.setData((Customer) user, new PaymentForm.BuyProductCallback() {
            @Override
            public void onPaymentSuccess() {
                buyProduct();
            }
        });
        Stage stage = new Stage();
        stage.setScene(new Scene(root));
        stage.setTitle("Payment Form");
        stage.setResizable(false);
        stage.show();
    }

    public void buyProduct()
    {
        Customer customer = (Customer) user;
        List<Product> products = cartProductList.getItems();
        shopHibernate.createCart(products, customer);
        cartProductList.getItems().clear();
        user = shopHibernate.getEntityById(User.class, user.getId());
        myOrdersListView.getItems().clear();
        if(user instanceof Customer)
            myOrdersListView.getItems().addAll(((Customer) user).getCustomerCarts());
        if(user instanceof Employee)
            myOrdersListView.getItems().addAll(((Employee) user).getManagedCarts());
    }

    //Store Tab
    public boolean checkStoreFields()
    {
        if(nameTextField.getText().isEmpty())
            return false;
        if(addressTextField.getText().isEmpty())
            return false;
        if(blacksmithComboBox.getSelectionModel().getSelectedItem() == null)
            return false;
        if(cityComboBox.getSelectionModel().getSelectedItem() == null)
            return false;
        return true;
    }

    public void clearStoreFields()
    {
        nameTextField.clear();
        addressTextField.clear();
        blacksmithComboBox.getSelectionModel().clearSelection();
        cityComboBox.getSelectionModel().clearSelection();
    }

    public void openStore(ActionEvent actionEvent)
    {
        if(!checkStoreFields())
        {
            showErrorPopup("Empty Fields");
        }
        Blacksmith blacksmith = blacksmithComboBox.getSelectionModel().getSelectedItem();
        PhysicalStore physicalStore = new PhysicalStore(
                nameTextField.getText(), addressTextField.getText(),
                cityComboBox.getSelectionModel().getSelectedItem()
        );
        physicalStore.setBlacksmith(blacksmith);
        shopHibernate.create(physicalStore);
        blacksmith.setPhysicalStore(physicalStore);
        shopHibernate.update(blacksmith);
        physicalStoreList.getItems().clear();
        physicalStoreList.getItems().addAll(shopHibernate.getAllRecords(PhysicalStore.class));
        clearStoreFields();
    }

    public void updateStore(ActionEvent actionEvent)
    {
        if(!checkStoreFields())
        {
            showErrorPopup("Empty Fields");
        }
        PhysicalStore physicalStore = shopHibernate.getEntityById(PhysicalStore.class, physicalStoreList.getSelectionModel().getSelectedItem().getId());
        physicalStore.setName(nameTextField.getText());
        physicalStore.setAddress(addressTextField.getText());
        physicalStore.setCity(cityComboBox.getSelectionModel().getSelectedItem());
        physicalStore.setBlacksmith(blacksmithComboBox.getSelectionModel().getSelectedItem());
        blacksmithComboBox.getSelectionModel().getSelectedItem().setPhysicalStore(physicalStore);
        shopHibernate.update(blacksmithComboBox.getSelectionModel().getSelectedItem());
        shopHibernate.update(physicalStore);
        physicalStoreList.getItems().clear();
        physicalStoreList.getItems().addAll(shopHibernate.getAllRecords(PhysicalStore.class));
        clearStoreFields();
    }

    public void closeStore(ActionEvent actionEvent)
    {
        Blacksmith blacksmith = physicalStoreList.getSelectionModel().getSelectedItem().getBlacksmith();
        shopHibernate.delete(PhysicalStore.class, physicalStoreList.getSelectionModel().getSelectedItem().getId());
        blacksmith.setPhysicalStore(null);
        shopHibernate.update(blacksmith);
        physicalStoreList.getItems().clear();
        physicalStoreList.getItems().addAll(shopHibernate.getAllRecords(PhysicalStore.class));
        clearStoreFields();

    }

    public void sendOrder(ActionEvent actionEvent)
    {
        if(user instanceof Customer)
            return;
        Cart cart = shopHibernate.getEntityById(Cart.class, myOrdersListView.getSelectionModel().getSelectedItem().getId());
        //I am sorry for that, but it works
        for(Product product : cart.getProducts()){
            product.getComments().clear();
            shopHibernate.update(product);
        }
        shopHibernate.deleteCartAndProducts(cart.getId());
        myOrdersListView.getItems().clear();
        user = shopHibernate.getEntityById(User.class, user.getId());
        myOrdersListView.getItems().addAll(((Employee) user).getManagedCarts());
        myOrdersProductsListView.getItems().clear();
    }

    public void declineOrder(ActionEvent actionEvent)
    {
        Cart cart = shopHibernate.getEntityById(Cart.class, myOrdersListView.getSelectionModel().getSelectedItem().getId());
        shopHibernate.deleteCart(cart.getId());
        myOrdersListView.getItems().clear();
        user = shopHibernate.getEntityById(User.class, user.getId());
        if(user instanceof Employee)
            myOrdersListView.getItems().addAll(((Employee) user).getManagedCarts());
        if(user instanceof Customer)
            myOrdersListView.getItems().addAll(((Customer) user).getCustomerCarts());
        myOrdersProductsListView.getItems().clear();
        resetFilters();
        productAdminList.getItems().clear();
        productAdminList.getItems().addAll(shopHibernate.loadAvailableProducts());
    }

    public void loadCartProductData(MouseEvent mouseEvent)
    {
        orderProductAdminList.getItems().clear();
        Cart cart = shopHibernate.getEntityById(Cart.class, orderAdminList.getSelectionModel().getSelectedItem().getId());
        orderProductAdminList.getItems().clear();
        orderProductAdminList.getItems().addAll(cart.getProducts());
    }

    public void previewProduct() throws IOException
    {
        Product product = productShopList.getSelectionModel().getSelectedItem();
        FXMLLoader fxmlLoader = new FXMLLoader(HelloApplication.class.getResource("product-preview.fxml"));
        Parent parent = fxmlLoader.load();
        ProductPreview productReview = fxmlLoader.getController();
        productReview.setData(entityManagerFactory, user, product);
        Stage stage = new Stage();
        stage.setScene(new Scene(parent));
        stage.setTitle(product.toString());
        stage.setResizable(false);
        stage.show();
    }

    public void sortPriceHighest(ActionEvent actionEvent)
    {
        List<Product> products = new ArrayList<>(productShopList.getItems()); // Create a copy
        Collections.sort(products, Comparator.comparingDouble(Product::getPrice).reversed());
        productShopList.getItems().clear();
        productShopList.getItems().addAll(products);

    }

    public void sortPriceLowest(ActionEvent actionEvent)
    {
        List<Product> products = new ArrayList<>(productShopList.getItems()); // Create a copy
        Collections.sort(products, Comparator.comparingDouble(Product::getPrice));
        productShopList.getItems().clear();
        productShopList.getItems().addAll(products);
    }

    public void resetFilters()
    {
        shopFixRadio.setSelected(false);
        shopFoldRadio.setSelected(false);
        shopAccessoryRadio.setSelected(false);
        productShopList.getItems().clear();
        productShopList.getItems().addAll(shopHibernate.loadAvailableProducts());
    }

    public void sortFoldingOnly(ActionEvent actionEvent)
    {
        productShopList.getItems().clear();
        productShopList.getItems().addAll(shopHibernate.loadAvailableProducts());
        List<Product> products = new ArrayList<>(productShopList.getItems());
        Iterator<Product> iterator = products.iterator();
        while (iterator.hasNext()) {
            Product product = iterator.next();
            if (!(product instanceof FoldingKnife)) {
                iterator.remove();
            }
        }
        productShopList.getItems().setAll(products);
    }

    public void sortFixedOnly(ActionEvent actionEvent)
    {
        productShopList.getItems().clear();
        productShopList.getItems().addAll(shopHibernate.loadAvailableProducts());
        List<Product> products = new ArrayList<>(productShopList.getItems());
        Iterator<Product> iterator = products.iterator();
        while (iterator.hasNext()) {
            Product product = iterator.next();
            if (!(product instanceof FixedKnife)) {
                iterator.remove();
            }
        }
        productShopList.getItems().setAll(products);
    }

    public void sortAccessoryOnly(ActionEvent actionEvent)
    {
        productShopList.getItems().clear();
        productShopList.getItems().addAll(shopHibernate.loadAvailableProducts());
        shopAccessoryRadio.setSelected(true);
        List<Product> products = new ArrayList<>(productShopList.getItems());
        Iterator<Product> iterator = products.iterator();
        while (iterator.hasNext()) {
            Product product = iterator.next();
            if ((product instanceof FoldingKnife) || (product instanceof FixedKnife)) {
                iterator.remove();
            }
        }
        productShopList.getItems().setAll(products);
    }

    public void updatePassword(ActionEvent actionEvent)
    {
        if(hash(oldPasswordTextField.getText()).equals(user.getPassword()))
        {
            if(newPasswordTextField.getText().equals(repeatNewPasswordTextField.getText()))
            {
                user.setPassword(hash(newPasswordTextField.getText()));
                shopHibernate.update(user);
            }
            else
                showErrorPopup("Passwords do not match");
        }
        else
            showErrorPopup("Password Incorrect");
        userAdminList.getItems().clear();
        userAdminList.getItems().addAll(shopHibernate.getAllRecords(User.class));
        oldPasswordTextField.clear();
        newPasswordTextField.clear();
        repeatNewPasswordTextField.clear();
    }

    public void logOut(ActionEvent actionEvent) throws IOException
    {
        FXMLLoader fxmlLoader = new FXMLLoader(HelloApplication.class.getResource("login-form.fxml"));
        Parent parent = fxmlLoader.load();
        Stage stage = (Stage) oldPasswordTextField.getScene().getWindow();
        stage.setScene(new Scene(parent));
        stage.setTitle("The Dwarven Forge");
        stage.setResizable(false);
        stage.show();
    }

    public void unnasignFromOrder(ActionEvent actionEvent)
    {
        Cart cart = myOrdersListView.getSelectionModel().getSelectedItem();
        cart.setEmployee(null);
        shopHibernate.update(cart);
        user = shopHibernate.getEntityById(User.class, user.getId());
        myOrdersListView.getItems().clear();
        myOrdersListView.getItems().addAll(((Employee) user).getManagedCarts());
        myOrdersProductsListView.getItems().clear();
        orderAdminList.getItems().clear();
        orderAdminList.getItems().addAll(shopHibernate.loadAvailableCarts());

    }

    public void openChat(ActionEvent actionEvent) throws IOException
    {
        Cart cart = myOrdersListView.getSelectionModel().getSelectedItem();
        FXMLLoader fxmlLoader = new FXMLLoader(HelloApplication.class.getResource("chat.fxml"));
        Parent parent = fxmlLoader.load();
        Chat chat = fxmlLoader.getController();
        chat.setData(entityManagerFactory, user, cart);
        Stage stage = new Stage();
        stage.setScene(new Scene(parent));
        stage.setTitle(cart.toString());
        stage.setResizable(false);
        stage.show();
    }

    public void loadStore(MouseEvent mouseEvent)
    {
        PhysicalStore physicalStore = shopHibernate.getEntityById(PhysicalStore.class, physicalStoreList.getSelectionModel().getSelectedItem().getId());
        productStoreList.getItems().clear();
        productStoreList.getItems().addAll(physicalStore.getProductList());
        employeeStoreList.getItems().clear();
        employeeStoreList.getItems().addAll(physicalStore.getEmployees());
        nameTextField.setText(physicalStore.getName());
        addressTextField.setText(physicalStore.getAddress());
        cityComboBox.setValue(physicalStore.getCity());
        blacksmithComboBox.setValue(physicalStore.getBlacksmith());
    }

    public void takeOrder(ActionEvent actionEvent)
    {
        Cart cart = orderAdminList.getSelectionModel().getSelectedItem();
        cart.setEmployee((Employee) user);
        shopHibernate.update(cart);
        myOrdersListView.getItems().add(cart);
        orderAdminList.getItems().clear();
        orderAdminList.getItems().addAll(shopHibernate.loadAvailableCarts());
        orderProductAdminList.getItems().clear();
    }

    public void loadMyOrderProducts(MouseEvent mouseEvent)
    {
        myOrdersProductsListView.getItems().clear();
        Cart cart = myOrdersListView.getSelectionModel().getSelectedItem();
        myOrdersProductsListView.getItems().addAll(cart.getProducts());
    }

    public void ordersPriceLowest(ActionEvent actionEvent)
    {
        List<Cart> carts = new ArrayList<>(orderAdminList.getItems());
        Collections.sort(carts, Comparator.comparingDouble(Cart::getTotalPrice));
        orderAdminList.getItems().clear();
        orderAdminList.getItems().addAll(carts);
    }

    public void ordersSortHighest(ActionEvent actionEvent)
    {
        List<Cart> carts = new ArrayList<>(orderAdminList.getItems());
        Collections.sort(carts, Comparator.comparingDouble(Cart::getTotalPrice).reversed());
        orderAdminList.getItems().clear();
        orderAdminList.getItems().addAll(carts);
    }

    public void filterByCustomerName(ActionEvent actionEvent)
    {
        resetOrdersFilters();
        List<Cart> carts = new ArrayList<>(orderAdminList.getItems());
        List<Cart> filteredCarts = new ArrayList<>();
        for (Cart cart : carts)
        {
            if((cart.getCustomer().getName() + " " + cart.getCustomer().getSurname()).equals(customerNameTextField.getText()))
                filteredCarts.add(cart);
            else if ((cart.getCustomer().getLogin()).equals(customerNameTextField.getText()))
            {
                filteredCarts.add(cart);
            }
        }
        orderAdminList.getItems().clear();
        orderAdminList.getItems().addAll(filteredCarts);
        customerNameTextField.clear();
    }

    public void resetOrdersFilters()
    {
        orderAdminList.getItems().clear();
        orderAdminList.getItems().addAll(shopHibernate.loadAvailableCarts());
    }
}
