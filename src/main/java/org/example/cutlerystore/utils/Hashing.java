package org.example.cutlerystore.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Hashing
{
    public static String hash(String input) {
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
            byte[] hash = messageDigest.digest(input.getBytes());
            StringBuilder stringBuilder = new StringBuilder();
            for (byte b : hash) {
                stringBuilder.append(String.format("%02x", b));
            }
            return stringBuilder.toString();
        } catch (NoSuchAlgorithmException e)
        {
            e.printStackTrace();
        }
        return input;
    }
}
