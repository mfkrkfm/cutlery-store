package org.example.cutlerystore.utils;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DatabaseOperations
{
    public static Connection connectToDb() throws ClassNotFoundException
    {
        Connection conn = null;
        Class.forName ("com.mysql.cj.jdbc.Driver");
        String DB_URL = "jdbc:mysql://localhost/test";
        String USER = "root";
        String PASS = "cutlerystore";
        try
        {
            conn = DriverManager.getConnection(DB_URL, USER, PASS) ;
        }
        catch (SQLException throwables)
        {
            throwables.printStackTrace();
        }
        return conn;
    }
    public static List<String> getProducts()
    {
        try
        {
            Connection connection = connectToDb();
            Statement statement = connection.createStatement();
            String sql = "SELECT * FROM test_table";
            ResultSet resultSet = statement.executeQuery(sql);
            List<String> products = new ArrayList<>();
            while(resultSet.next())
            {
                products.add(resultSet.getString(1) + " " + resultSet.getInt(2));
            }
            return products;

        } catch (ClassNotFoundException | SQLException e)
        {
            throw new RuntimeException(e);
        }
    }

    public static void main(String[] args)
    {
        List<String> res = getProducts();
        for(String i : res)
        {
            System.out.println(i);
        }
    }
}
