package org.example.cutlerystore.hibernate;

import jakarta.persistence.*;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Root;
import org.example.cutlerystore.store.*;
import org.example.cutlerystore.store.product.Product;

import java.util.ArrayList;
import java.util.List;

public class ShopHibernate extends GenericHibernate
{
    public ShopHibernate(EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    public void createCart(List<Product> products, User user) {
        try {
            entityManager = getEntityManager();
            entityManager.getTransaction().begin();
            Customer buyer = entityManager.find(Customer.class, user.getId());
            Cart cart = new Cart(buyer, new ArrayList<>());
            for (Product p : products) {
                Product product = entityManager.find(Product.class, p.getId());
                product.setCart(cart);
                cart.getProducts().add(product);
                cart.setTotalPrice(cart.getTotalPrice() + product.getPrice());
            }
            buyer.getCustomerCarts().add(cart);
            entityManager.merge(buyer);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (entityManager != null) entityManager.close();
        }
    }

    public void deleteCart(int cartId) {
        List<Comment> comments = new ArrayList<>();
        EntityManager entityManager = null;
        try {
            entityManager = getEntityManager();
            entityManager.getTransaction().begin();
            Cart cart = entityManager.find(Cart.class, cartId);
            if (cart != null)
            {
                for (Product product : cart.getProducts())
                {
                    product.setCart(null);
                    entityManager.merge(product);
                }
                for (Comment comment : cart.getChat())
                {
                    if (comment != null)
                    {
                        comment.setChat(null);
                        comment.setOwner(null);
                        entityManager.merge(comment);
                        comments.add(comment);
                    }
                }
                Customer customer = cart.getCustomer();
                if (customer != null)
                {
                    customer.getCustomerCarts().remove(cart);
                    entityManager.merge(customer);
                }
                Employee employee = cart.getEmployee();
                if (employee != null) {
                    employee.getManagedCarts().remove(cart);
                    entityManager.merge(employee);
                }
                entityManager.remove(cart);
            }
            entityManager.getTransaction().commit();
            deleteChatComments(comments);
        } catch (Exception e) {
            if (entityManager != null && entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().rollback();
            }
            e.printStackTrace();
        } finally {
            if (entityManager != null) {
                entityManager.close();
            }
        }
    }

    public void deleteCartAndProducts(int cartId) {
        List<Comment> comments = new ArrayList<>();
        List<Product> products = new ArrayList<>();
        Cart c = getEntityById(Cart.class, cartId);
        EntityManager entityManager = null;
        try {
            entityManager = getEntityManager();
            entityManager.getTransaction().begin();
            Cart cart = entityManager.find(Cart.class, cartId);
            if (cart != null)
            {
                for (Product product : cart.getProducts())
                {
                    product.setCart(null);
                    product.setPhysicalStore(null);
                    entityManager.merge(product);
                    products.add(product);
                }
                for (Comment comment : cart.getChat())
                {
                    if (comment != null)
                    {
                        comment.setChat(null);
                        comment.setOwner(null);
                        entityManager.merge(comment);
                        comments.add(comment);
                    }
                }
                Customer customer = cart.getCustomer();
                if (customer != null)
                {
                    customer.getCustomerCarts().remove(cart);
                    entityManager.merge(customer);
                }
                Employee employee = cart.getEmployee();
                if (employee != null) {
                    employee.getManagedCarts().remove(cart);
                    entityManager.merge(employee);
                }
                entityManager.remove(cart);
            }
            entityManager.getTransaction().commit();
            deleteChatComments(comments);
            deleteCartProducts(products);
        } catch (Exception e) {
            if (entityManager != null && entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().rollback();
            }
            e.printStackTrace();
        } finally {
            if (entityManager != null) {
                entityManager.close();
            }
        }
    }

    public List<Cart> loadAvailableCarts(){
        EntityManager em = getEntityManager();
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Cart> query = cb.createQuery(Cart.class);
            Root<Cart> root = query.from(Cart.class);
            query.select(root).where(cb.isNull(root.get("employee")));
            Query q;
            q = em.createQuery(query);
            return q.getResultList();
        } catch (NoResultException e) {
            return null;
        } finally {
            if (em != null) em.close();
        }
    }

    public List<Product> loadAvailableProducts(){
        EntityManager em = getEntityManager();
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Product> query = cb.createQuery(Product.class);
            Root<Product> root = query.from(Product.class);
            query.select(root).where(cb.isNull(root.get("cart")));
            Query q;

            q = em.createQuery(query);
            return q.getResultList();
        } catch (NoResultException e) {
            return null;
        } finally {
            if (em != null) em.close();
        }
    }

    public List<Blacksmith> loadBlacksmiths(){
        EntityManager em = getEntityManager();
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<User> query = cb.createQuery(User.class);
            Root<User> root = query.from(User.class);
            query.select(root).where(cb.equal(root.type(), Blacksmith.class));
            Query q;

            q = em.createQuery(query);
            return q.getResultList();
        } catch (NoResultException e) {
            return null;
        } finally {
            if (em != null) em.close();
        }
    }

    public <T> T getUserByCredentials(String login, String password) {
        EntityManager em = getEntityManager();
        try {
            Employee employee = getEmployeeByCredentials(login, password);
            return employee == null ? (T) getCustomerByCredentials(login, password) : (T) employee;
        } catch (NoResultException e) {
            return null;
        } finally {
            if (em != null) em.close();
        }
    }

    public Employee getEmployeeByCredentials(String login, String password) {
        EntityManager em = getEntityManager();
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Employee> query = cb.createQuery(Employee.class);
            Root<Employee> root = query.from(Employee.class);
            query.select(root).where(cb.and(cb.like(root.get("login"), login), cb.like(root.get("password"), password)));
            Query q;

            q = em.createQuery(query);
            return (Employee) q.getSingleResult();
        } catch (NoResultException e) {
            return null;
        } finally {
            if (em != null) em.close();
        }
    }

    public Customer getCustomerByCredentials(String login, String password) {
        EntityManager em = getEntityManager();
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Customer> query = cb.createQuery(Customer.class);
            Root<Customer> root = query.from(Customer.class);
            query.select(root).where(cb.and(cb.like(root.get("login"), login), cb.like(root.get("password"), password)));
            Query q;

            q = em.createQuery(query);
            return (Customer) q.getSingleResult();
        } catch (NoResultException e) {
            return null;
        } finally {
            if (em != null) em.close();
        }
    }

    public void deleteComment(int id) {
        EntityManager em = getEntityManager();
        try {
            em.getTransaction().begin();
            var comment = em.find(Comment.class, id);
            if (comment.getProduct() != null) {
                Product product = em.find(Product.class, comment.getProduct().getId());
                product.getComments().remove(comment);
                em.merge(product);
            }

            User user = getUserByCredentials(comment.getOwner().getLogin(), comment.getOwner().getPassword());
            user.getComments().remove(comment);
            em.merge(user);

            if(comment.getReplies() != null)
                comment.getReplies().clear();

            if(comment.getParentComment() != null)
                comment.getParentComment().getReplies().remove(comment);

            em.remove(comment);

            em.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteChatComments(List<Comment> comments) {
        EntityManager em = getEntityManager();
        try {
            em.getTransaction().begin();
            for (Comment comment : comments) {
                Comment comm = em.merge(comment);
                em.remove(comm);
            }
            em.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteCartProducts(List<Product> products)
    {
        EntityManager em = getEntityManager();
        try {
            em.getTransaction().begin();
            for (Product product : products) {
                product.getComments().clear();
                Product prod = em.merge(product);
                em.remove(prod);
            }
            em.getTransaction().commit();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }





}
